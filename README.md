# mathewparet/requires-publishing

Provide the ability to models to be published.

## Features

1. Publish / Unpublish a model.
2. Published models are not considered for any queries by default (adds a global scope).

# Installation

```shell
composer require mathewparet/requires-publishing
```

# Defining a field to hold publish status in migrations

```php
// define a 'published_at' field of type dateTime and is nullable
$table->requiresPublishing();
```

or


```php
// define a 'active_at' field of type dateTime and is nullable
$table->requiresPublishing('active_at');
```

# Add the funtionality to your model

```php
// ...
use mathewparet\RequiresPublishing\RequiresPublishing;
// ...
class User extends Authenticatable
{
    // ...
    use RequiresPublishing;
    // ...

    /**
     * To use custom field name for publishing, use the below line. If the below line isn't 
     * defined, the field is assumed to be 'published_at'.
     */
    const PUBLISHED_AT = 'active_at';
}
```

# API referance

This package introduces the below methods:

| Name | Availability | Description |
|---|---|---|
`requiresPublishing()`|Blueprint|Macro to define a field that holds the publish state for the record.|
|`publish()`|Model|Mark a model as published.|
|`publishQuetly()`|Model|Mark a model as published without raising any events.|
|`unpublish()`|Model, Builder|Mark a model as unpublished.|
|`unpublishQuetly()`|Model|Mark a model as unpublished without raising any events.|
|`withUnpublished()`|Model|Add unpublished records to the scope.|
|`withoutUnpublished()`|Model|Remove unpublished records from scope.|
|`onlyUnpublished()`|Model|Scope the query to only consider unpublished records.|


# Events

1. `suspending`
2. `suspneded`
3. `unsuspending`
4. `unsuspended`