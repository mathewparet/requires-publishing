<?php
namespace mathewparet\RequiresPublishing;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Blueprint;


class PublishingServiceProvider extends ServiceProvider
{
    /**
     * Register service
     */
    public function register(): void
    {
        // 
    }

    /**
     * Bootstrap services
     */
    public function boot(): void
    {
        Blueprint::macro("requiresPublishing", function($name = 'published_at') {
            /**
             * @var \Illuminate\Database\Schema\Blueprint $this
             */
            return $this->dateTime($name)->nullable();
        });
    }
}

