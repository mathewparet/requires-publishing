<?php
namespace mathewparet\RequiresPublishing;

/**
 * @var string SUSPENDED_AT
 */
trait RequiresPublishing
{
    /**
     * Boot the RequiresPublishing trait for a model.
     *
     * @return void
     */
    public static function bootRequiresPublishing()
    {
        static::addGlobalScope(new PublishingScope);
    }

    /**
     * Initialize the RequiresPublishing trait for an instance.
     *
     * @return void
     */
    public function initializeRequiresPublishing()
    {
        if (! isset($this->casts[$this->getPublishedAtColumn()])) {
            $this->casts[$this->getPublishedAtColumn()] = 'datetime';
        }
    }

    /**
     * Publish a model
     *
     * @return bool|null
     */
    public function publish()
    {
        if ($this->fireModelEvent('publishing') === false) {
            return false;
        }
        return tap($this->runPublishing(), function ($published) {
            if ($published) {
                $this->fireModelEvent('published', false);
            }
        });
    }

    /**
     * Perform the actual publishing query on this model instance.
     *
     * @return mixed
     */
    protected function performPublishOnModel()
    {
        return $this->runPublishing();
    }

    /**
     * Perform the actual publishing query on this model instance.
     *
     * @return void
     */
    protected function runPublishing()
    {
        $query = $this->setKeysForSaveQuery($this->newModelQuery());

        $time = $this->freshTimestamp();

        $columns = [$this->getPublishedAtColumn() => $this->fromDateTime($time)];

        $this->{$this->getPublishedAtColumn()} = $time;

        if ($this->usesTimestamps() && ! is_null($this->getUpdatedAtColumn())) {
            $this->{$this->getUpdatedAtColumn()} = $time;

            $columns[$this->getUpdatedAtColumn()] = $this->fromDateTime($time);
        }

        $query->update($columns);

        $this->syncOriginalAttributes(array_keys($columns));

        $this->fireModelEvent('published', false);
    }

    /**
     * Publish model without raising any events.
     *
     * @return bool|null
     */
    public function publishQueitly()
    {
        return static::withoutEvents(fn () => $this->runPublishing());
    }

    /**
     * Register a "publishing" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function publishing($callback)
    {
        static::registerModelEvent('publishing', $callback);
    }

    /**
     * Register a "published" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function published($callback)
    {
        static::registerModelEvent('published', $callback);
    }

    /**
     * Determine if the model instance has been published.
     *
     * @return bool
     */
    public function isPublished()
    {
        return ! is_null($this->{$this->getPublishedAtColumn()});
    }

    /**
     * Unpublish model instance.
     *
     * @return bool
     */
    public function unpublish()
    {
        // If the unpublishing event does not return false, we will proceed with this
        // unpublish operation. Otherwise, we bail out so the developer will stop
        // the unpublish totally. We will clear the published timestamp and save.
        if ($this->fireModelEvent('unpublishing') === false) {
            return false;
        }

        $this->{$this->getPublishedAtColumn()} = null;

        // Once we have saved the model, we will fire the "unpublished" event so this
        // developer will do anything they need to after a unpublish operation is
        // totally finished. Then we will return the result of the save call.
        $this->exists = true;

        $result = $this->save();

        $this->fireModelEvent('unpublished', false);

        return $result;
    }

    /**
     * Unpublish model instance without raising any events.
     *
     * @return bool
     */
    public function unpublishQuietly()
    {
        return static::withoutEvents(fn () => $this->unpublish());
    }

    /**
     * Register a "unpublishing" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function unpublishing($callback)
    {
        static::registerModelEvent('unpublishing', $callback);
    }

    /**
     * Register a "unpubished" model event callback with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function unpubished($callback)
    {
        static::registerModelEvent('unpubished', $callback);
    }

    /**
     * Get the name of the "published at" column.
     *
     * @return string
     */
    public function getPublishedAtColumn()
    {
        return defined(static::class.'::PUBLISHED_AT') ? static::PUBLISHED_AT : 'published_at';
    }

    /**
     * Get the fully qualified "published at" column.
     *
     * @return string
     */
    public function getQualifiedPublishedAtColumn()
    {
        return $this->qualifyColumn($this->getPublishedAtColumn());
    }
}